//
//  AppDelegate.h
//  06_Data_Types
//
//  Created by Admin on 30.12.16.
//  Copyright © 2016 Nesynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

